from GameState import GameState
from random import randint
from Directions import *

class SearchSolver(object):
    '''
    classdocs
    '''


    def __init__(self, inDepth):
        '''
        Constructor
        '''
        self.depth = inDepth
        self.numMoves = 0
        self.name = "SearchSolver"
        self.lastMove = 0
        pass
    
    
    def makeMove(self, game):
        results = self.startSearch(GameState(game), game.copyArr(), self.depth)
        game.takeMove(results[0])
        self.lastMove = results[0]
        return game
    
    
    # determines the move with the highest ev with the given search depth
    def startSearch(self, game, board, depth):
        bestOption = None
        bestVal = -1
        
        # base case, depth == 0
        # in this case, estimate rest of the way greedy?
        if (depth <= 0):
            return (Move.up, 0)
        
        upVal = self.estimateValue(game, depth - 1, board, Move.up)
        if (upVal > bestVal):
            bestOption = Move.up
        
        rightVal = self.estimateValue(game, depth - 1, board, Move.right)
        if (rightVal > bestVal):
            bestOption = Move.right
        
        return (bestOption, bestVal)
    
    
    def estimateValue(self, game, depth, board, move):
        ev2 = [[0 for x in range(4)] for x in range(4)]
        ev4 = [[0 for x in range(4)] for x in range(4)]
        ev = 0
        numOptions = 0
        
        # go through all options
        if(game.isValid(move)): 
            baseGameUp = GameState()
            baseGameUp.setBoard(board)
            baseGameUp.executeMove(move)
            
            for x in range (0, 4):
                for y in range (0, 4):
                    if(baseGameUp.gameArray[x][y] == 0):
                        numOptions += 2
                        
                        # per cell ev expecting 4
                        pretendBoard2 = baseGameUp.copyArr()
                        pretendGame2 = GameState()
                        pretendBoard2[x][y] = 2
                        pretendGame2.setBoard(pretendBoard2)
                        searchEv = self.startSearch(game, pretendGame2.copyArr(), depth)
                        ev2[x][y] = searchEv[1]
                        pretendGame4 = None
                    
                        # per cell ev expecting 4
                        pretendBoard4 = baseGameUp.copyArr()
                        pretendGame4 = GameState()
                        pretendBoard4[x][y] = 4
                        pretendGame4.setBoard(pretendBoard4)
                        searchEv = self.startSearch(game, pretendGame4.copyArr(), depth)
                        ev4[x][y] = searchEv[1]
                        pretendGame4 = None
                    
                        pass
        return ev
    
    
    # Returns the number of available moves
    def getMoves(self):
        return self.numMoves
    
    # Returns the name of the solver
    def getName(self):
        return self.name

    # Returns the last move of the solver
    def getLastMove(self):
        return self.lastMove
