from GameState import GameState
from random import randint
from Directions import *
from ValueCalculator import value
import time
import datetime

class SearchRandomComparison(object):
    '''
    classdocs
    '''


    def __init__(self, inDepth, zeroes):
        '''
        Constructor
        '''
        self.numMoves = 0
        self.depth = inDepth
        self.zeroes = zeroes
        self.disagreements = 0
        self.comparisons = 0
        self.lastMove = 0
        self.name = "SearchRandomComparison"
        pass
    
    
    # keeps searching for moves until the game is complete
    def makeMove(self, game):
        count = 0
        disagreements = 0
        comparisons = 0
        
        bestMove = self.searchRandom(GameState(game), game.copyArr(), self.depth)
            
        if self.enoughZeroes(game):
            self.comparisons += 1
            noRandomMove = self.searchNoRandom(game, game.copyArr(), self.depth)
            if not noRandomMove == bestMove:
                self.disagreements += 1
            
            
        # when at the end, all decisions might lead to an inevitable failure
        if (not game.isValid(bestMove)):
            return None 

        game.takeMove(bestMove[0])
        self.lastMove = bestMove[0]
        self.numMoves = self.numMoves + 1
        
        return game
    
    
    # determines whether or not a comparison should occur based on how full the board is
    # number of required 0s is determined at solver creation
    def enoughZeroes(self, game):
        numZeroes = 0
        
        # count number of 0-tiles
        for x in range (0, 4):
            for y in range (0, 4):
                if game.gameArray[x][y] == 0:
                    numZeroes += 1
        
        if numZeroes >= self.zeroes:
            return True
        return False
    
    
    # returns best move and the value of that move factoring in random tiles
    # best move is only useful for the top-level call
    def searchRandom(self, game, board, depth):
        if (depth == 0):
            return (Move.up, 0)
        
        bestMove = Move.up
        bestValue = -1
        
        move = Move.up
        moveValue = self.searchDirectionRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        move = Move.left
        moveValue = self.searchDirectionRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
            
        move = Move.right
        moveValue = self.searchDirectionRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        move = Move.down
        moveValue = self.searchDirectionRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        return (bestMove, bestValue)
    
    
    # returns the number of matches that a given move would make
    # this only determines value of one move and no further searching
    def valueOfMove(self, game, board, move):
        return value(game.preRotate(move, board), game, move)
    
    
    # returns the expected value of a given move searching with the given depth factoring in random tiles
    def searchDirectionRandom(self, game, board, depth, move):
        testGame = GameState(game)
        
        # if the move isn't valid, don't consider it
        if (not testGame.isValid(move)):
            return -1
        
        #determine the value for making the move at this level
        ourValue = self.valueOfMove(testGame, testGame.gameArray, move)
        
        #'using that as the starting board, check a lot of possibilities'
        afterMove = testGame.executeMove(move)
        testGame.setBoard(afterMove)
        ev2 = [[0 for x in range(4)] for x in range(4)]
        ev4 = [[0 for x in range(4)] for x in range(4)]
        
        options = 0
        searchValue = 0
        
        # determine the value of each cell
        for x in range (0, 4):
            for y in range (0, 4):
                trialBoard = testGame.copyArr()
                if (trialBoard[x][y] == 0):
                    options += 1
                    
                    trialBoard[x][y] = 2
                    ev2[x][y] = self.searchRandom(testGame, trialBoard, depth - 1)[1]
                    trialBoard[x][y] = 0
                    
                    trialBoard[x][y] = 4
                    ev4[x][y] = self.searchRandom(testGame, trialBoard, depth - 1)[1]
                    trialBoard[x][y] = 0
        
        
        # adjust those cells for their likelihood
        for x in range (0, 4):
            for y in range (0, 4):
                searchValue += (ev2[x][y] * 0.9) / options
                searchValue += (ev4[x][y] * 0.1) / options
        
        return ourValue + searchValue
    
    
    # returns best move and the value of that move
    # best move is only useful for the top-level call
    def searchNoRandom(self, game, board, depth):    
        if (depth == 0):
            return (Move.up, 0)
        
        bestMove = Move.up
        bestValue = -1
        
        move = Move.up
        moveValue = self.searchDirectionNoRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        move = Move.left
        moveValue = self.searchDirectionNoRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
            
        move = Move.right
        moveValue = self.searchDirectionNoRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        move = Move.down
        moveValue = self.searchDirectionNoRandom(game, board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        return (bestMove, bestValue)


    # returns the expected value of a given move searching with the given depth
    # this ignores the new tiles appearing, which saves tons on complexity
    def searchDirectionNoRandom(self, game, board, depth, move):
        testGame = GameState(game)
        
        # if the move isn't valid, don't consider it
        if (not testGame.isValid(move)):
            return -1
        
        # determine the value for making the move at this level
        ourValue = self.valueOfMove(testGame, testGame.gameArray, move)
        
        # using that as the starting board, check the child's options
        afterMove = testGame.executeMove(move)
        #testGame.setBoard(afterMove)
        
        #trialBoard = testGame.copyArr()
        searchValue = self.searchNoRandom(game, afterMove, depth - 1)[1]
        
        return ourValue + searchValue


    # class specific stats
    def getComparisons(self):
        return self.comparisons
    
    def getDisagreements(self):
        return self.disagreements

    def getMoves(self):
        return self.numMoves
    
    # Returns the last move made
    def getLastMove(self):
        return self.lastMove

    # Returns the solver's name
    def getName(self):
        return self.name
