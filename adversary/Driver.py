from RandomSolver import RandomSolver
from GreedySolver import GreedySolver
from GreedySearch import GreedySearch
from RegionSearch import RegionSearch
from GreedySearchNoRandom import GreedySearchNoRandom
from GreedySearchPercentageDepth import GreedySearchPercentageDepth
from SearchSolver import SearchSolver
from SearchRandomComparison import SearchRandomComparison
from Directions import *
from GameState import GameState
from Histogram import DataGroup
import sys
import time
import datetime

# helps determine when the strategy should be switched
factor = 0.5

def printCommandLine():
   print("python3 Driver.py strategy-file num-trials [output-file [log-file]]")

def parseStratFile(fileName):
   stratFile = open(fileName, 'r')
   solverList = list()
   for line in stratFile:
      splitLine = line.split()

      if ('RandomSolver' == splitLine[0] and 1 == len(splitLine)):
         solverList.append(RandomSolver())

      elif ('GreedySolver' == splitLine[0] and 1 == len(splitLine)):
         solverList.append(GreedySolver())

      elif ('GreedySearch' == splitLine[0] and 2 == len(splitLine)):
         solverList.append(GreedySearch(int(splitLine[1])))

      elif ('GreedySearchNoRandom' == splitLine[0] and 2 == len(splitLine)):
         solverList.append(GreedySearchNoRandom(int(splitLine[1])))

      elif ('GreedySearchPercentageDepth' == splitLine[0] and 2 == len(splitLine)):
         solverList.append(GreedySearchPercentageDepth(int(splitLine[1])))

      elif ('RegionSearch' == splitLine[0] and 2 == len(splitLine)):
         solverList.append(RegionSearch(int(splitLine[1])))

      elif ('SearchRandomComparison' == splitLine[0] and 3 == len(splitLine)):
         solverList.append(SearchRandomComparison(int(splitLine[1]), int(splitLine[2])))

      elif ('SearchSolver' == splitLine[0] and 2 == len(splitLine)):
         solverList.append(SearchSolver(int(splitLine[1])))

      else:
         sys.exit("Error: could not parse strategy file {}".format(line))

   return solverList

def checkChangeStrategy(bestNextGame, curNextGame):
   # TODO: move eval function to every solver
   if (None != curNextGame and 
       curNextGame.getScore() - bestNextGame.getScore() > factor * bestNextGame.getScore()):
      return True

   return False
   

def main():

   # check command line for valid length
   if (3 > len(sys.argv)) or (5 < len(sys.argv)):
      printCommandLine()
      return

   # collect info from sys.argv
   strategy_filename = sys.argv[1]
   num_trials = int(sys.argv[2])
   output_filename = ''
   log_filename = ''

   outputFileStream = sys.stdout # if output file specified, will point to that file
   logFileStream = sys.stdout # if log file specified, will point to that file


   # collect info for optional args output file and log file
   if (4 <= len(sys.argv)):
      output_filename = sys.argv[3]
      if (5 == len(sys.argv)):
         log_filename = sys.argv[4]

   # get the list of solvers from the strategy file
   solvers = parseStratFile(strategy_filename)

   # if output file provided, open
   if ('' != output_filename):
      outputFileStream = open(output_filename, 'w+')

      # if log file provided, open
      if ('' != log_filename):
         logFileStream = open(log_filename, 'w+')

   # if log file specified, list all solvers
   if (sys.stdout != logFileStream):
      logFileStream.write('Solvers:\n')
      for i in range(0, len(solvers)):
         logFileStream.write('{}\n'.format(solvers[i].getName()))
      logFileStream.write('\n\n')

   bestScore = [0, 0] # saves the highest-scoring game and its score

   # run all games
   for gameNum in range(1, num_trials + 1):
      solver = 0 
      bestSolver = 0
      moveNum = 1
      tile = DataGroup()
      score = DataGroup()
      game = GameState()

      
      outputFileStream.write('Game {}\n\n'.format(gameNum))
      game.printState(outputFileStream)
      outputFileStream.write('\n')

      if (sys.stdout != logFileStream):
         logFileStream.write('Game {}\n\n'.format(gameNum))
         game.printState(logFileStream)
         logFileStream.write('\n')


      while (game.isGoing()):
         # find the first strategy in solvers that can make a move; mark this as bestNextGame
         bestNextGame = GameState(game) # saves the current best next move
         bestNextGame = solvers[solver].makeMove(bestNextGame)

         while (None == bestNextGame and len(solvers) - 1 > solver):
            solver += 1
            bestSolver = solver
            bestNextGame = GameState(game)
            bestNextGame = solvers[solver].makeMove(bestNextGame)

         solver += 1

         if (None == bestNextGame):
            # No strategy could make a move; exit
            print("Game ended abnormally\n\n")
            break
            
         # if bestSolver is not the last solver, check solvers after bestSolver
         if (len(solvers) - 1 >= solver):
            # check the solver adjacent to bestSolver
            curNextGame = GameState(game) # the current game being tested against bestNextGame
            curNextGame = solvers[solver].makeMove(curNextGame)

            # take the best move
            while (checkChangeStrategy(bestNextGame, curNextGame) and len(solvers) - 1 > solver):
               # if the solver adjacent to bestSolver scored better than bestSolver, set bestSolver to the
               # adjacent solver and test the next one
               # continue this process until the solver next to bestSolver performs worse than bestSolver or
               # until the end of solvers is reached

               bestNextGame = curNextGame
               bestSolver = solver

               solver += 1
               curNextGame = GameState(game)
               curNextGame = solvers[solver].makeMove(curNextGame)

            if (None != curNextGame and bestNextGame.getScore() < curNextGame.getScore()):
               # check to see if ran off the end of solvers or if curNextGame is better than bestNextGame
               bestNextGame = curNextGame
               bestSolver = solver

         # Add random tile
         bestNextGame.addRandomTile()

         # print results
         outputFileStream.write('Game {} turn {}\n'.format(gameNum, moveNum))
         outputFileStream.write('Score: {}\n'.format(bestNextGame.getScore()))
         bestNextGame.printState(outputFileStream)
         outputFileStream.write('\n')

         if (sys.stdout != logFileStream):
            logFileStream.write('Game {} turn {}\n'.format(gameNum, moveNum))
            logFileStream.write('Score: {}\n'.format(bestNextGame.getScore()))
            logFileStream.write('Solver: {}\n'.format(solvers[bestSolver].getName()))
            logFileStream.write('Move: {}\n'.format(Move(solvers[bestSolver].getLastMove()))) 
            bestNextGame.printState(logFileStream)
            logFileStream.write('\n')
         
         solver = 0 
         bestSolver = 0
         game = bestNextGame

         moveNum += 1

      outputFileStream.write("Score: {}\n".format(bestNextGame.getScore()))
      if (sys.stdout != logFileStream):
         logFileStream.write("Score: {}\n".format(bestNextGame.getScore()))

      if (bestNextGame.getScore() > bestScore[1]):
         bestScore[0] = gameNum
         bestScore[1] = bestNextGame.getScore()

   outputFileStream.write('Best score: {} (game {})\n'.format(bestScore[1], bestScore[0]))
   if (sys.stdout != logFileStream):
      logFileStream.write('Best score: {} (game {})\n'.format(bestScore[1], bestScore[0]))
   pass

if __name__ == '__main__':
   main()
