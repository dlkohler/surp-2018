from GameState import *
from random import randint
from Directions import *
import sys
import time
import datetime

class GreedySolver(object):
    fourCorners = {(0, 0), (0, 3), (3, 0), (3, 3)}
    possibilities = {Move.down, Move.left, Move.up, Move.right}
    
    stayInCorner = 0.5
    enterCorner = 1.5
    moveDownMultiplier = 0.8


    def __init__(self):
        self.numMoves = 0
        self.lastMove = 0
        self.name = "GreedySolver"
        pass
    
    # Makes a single move
    def makeMove(self, game):
        testBoard = game.copyArr()
         
        bestOption = None
        bestValue = -1
        possibilities = {Move.down, Move.left, Move.up, Move.right}
         
        moveValue = 0
         
        for option in possibilities:
            if (game.isValid(option)):
                moveVal = self.testDir(GameState(game), option)
                if (moveValue > bestValue):
                    bestValue = moveValue
                    bestOption = option
         
        game.takeMove(bestOption)
        self.lastMove = bestOption
        return game
    
    # Shows if there is a valid move in the given direction
    def testDir(self, game, direction):
        testBoard = game.copyArr()
        game.preRotate(direction, testBoard)
        testGame = GameState(game)
        dirVal = 0
        
        for x in range(0, 4):
            dirVal += countSlideDownMatches(x, testBoard)
        
        # check if the largest tile is in a corner before the move
        # if it isn't, moving it to the corner is worth a lot
        inCorner = False
        maxTile = testGame.getMaxTile()
        for corner in self.fourCorners:
            if testGame.gameArray[corner[0]][corner[1]] == maxTile:
                inCorner = True
        
        # check where it is after the move
        maxTile = testGame.getMaxTile()
        testGame.executeMove(Move.down)
        for corner in self.fourCorners:
            if testGame.gameArray[corner[0]][corner[1]] == maxTile:
                if not inCorner:
                    dirVal += self.enterCorner
                else:
                    dirVal += self.stayInCorner
        
        # penalty for moving down
        if direction == Move.down:
            dirVal *= self.moveDownMultiplier

        return dirVal
    
    # Returns the number of available moves
    def getMoves(self):
        return self.numMoves

    # Returns the last move made
    def getLastMove(self):
        return self.lastMove

    # Returns the solver's name
    def getName(self):
        return self.name
