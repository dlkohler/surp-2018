from GameState import GameState
from random import randint
from ValueCalculator import value
from Directions import *

class GreedySearchNoRandom(object):

    def __init__(self, inDepth):
        self.numMoves = 0
        self.depth = inDepth
        self.lastMove = 0
        self.name = "GreedySearchNoRandom"
        pass
    
    
    # keeps searching for moves until the game is complete
    def makeMove(self, game):
        count = 0
        
        testBoard = game.copyArr()
            
        bestMove = self.search(testBoard, self.depth)
            
        # when at the end, all decisions might lead to an inevitable failure
        if (not game.isValid(bestMove)):
            # TODO: use this as a condition to change solvers
            return None
         
        game.takeMove(bestMove[0])
        self.lastMove = bestMove[0]
        return game
    
    
    # returns best move and the value of that move
    # best move is only useful for the top-level call
    def search(self, board, depth):    
        if (depth == 0):
            return (Move.up, 0)
        
        bestMove = Move.up
        bestValue = -1
        
        move = Move.up
        moveValue = self.searchDirection(board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        move = Move.left
        moveValue = self.searchDirection(board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
            
        move = Move.right
        moveValue = self.searchDirection(board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        move = Move.down
        moveValue = self.searchDirection(board, depth, move)
        if (moveValue > bestValue):
            bestMove = move
            bestValue = moveValue
        
        return (bestMove, bestValue)
    
    
    # returns the number of matches that a given move would make
    # this only determines value of one move and no further searching
    def valueOfMove(self, game, board, move):
        return value(game.preRotate(move, board), game, move)
    
    
    # returns the expected value of a given move searching with the given depth
    # this ignores the new tiles appearing, which saves tons on complexity
    def searchDirection(self, board, depth, move):
        testGame = GameState()
        testGame.setBoard(board)
        testGame.setBoard(testGame.copyArr())
        
        # if the move isn't valid, don't consider it
        if (not testGame.isValid(move)):
            return -1
        
        # determine the value for making the move at this level
        ourValue = self.valueOfMove(testGame, testGame.gameArray, move)
        
        # using that as the starting board, check the child's options
        afterMove = testGame.executeMove(move)
        searchValue = self.search(afterMove, depth - 1)[1]
        
        return ourValue + searchValue
    
    
    # returns the number of moves
    def getMoves(self):
        return self.numMoves
    
    # Returns the last move made
    def getLastMove(self):
        return self.lastMove

    # Returns the solver's name
    def getName(self):
        return self.name
