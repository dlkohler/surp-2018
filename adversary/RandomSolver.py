from GameState import GameState
from random import randint
from Directions import *

class RandomSolver(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.numMoves = 0
        self.lastMove = 0
        self.name = "RandomSolver"
        pass

    # Makes a single move
    def makeMove(self, game):
        # pick a move
        move = randint(1, 4)
        # execute move
        while (not game.isValid(Move(move))):
            move = randint(1, 4)
        game.takeMove(Move(move))
        self.numMoves += 1
        self.lastMove = move
        return game

    '''
    # returns true if the next game 
    def eval(self, curGame, nextGame):

    '''
    
    
    # Returns the number of available moves
    def getMoves(self):
        return self.numMoves
    
    # Returns the last move made
    def getLastMove(self):
        return self.lastMove

    # Returns the solver's name
    def getName(self):
        return self.name
