from Directions import *
from GameState import *
from Evaluators import *
from sys import stdout

# A node in the tree
class Tree:
   # fields:
   #  game: the GameState contained by the node
   #  children: a list of the node's chidlren (also Trees). Initialize with a
   #            list (never None)
   #  prevMove: the move that was used to get from the previous turn to the
   #            the node's GameState
   # prob: the probability of reaching this game from its parent

   def __init__(self, game, children, prevMove, prob):
      self.game = game
      self.children = children
      self.prevMove = prevMove
      self.prob = prob

### Helper methods -- don't call these from outside Tree.py ###

# Given a game's array and a value, calculates the probability of a tile of the
# value being placed in a specific empty place
def probOfNewTile(gameArray, value):
   if 2 == value:
      return .9 / countEmpty(gameArray)
   return .1 / countEmpty(gameArray)

# Given a node, generates the level below it
def genLevel(node, opt):
   for move in range(1, 5):
      if node.game.isValid(Move(move)):
         moveGame = GameState(node.game)
         moveGame.takeMove(Move(move))
         gameArray = moveGame.copyArr()
         # generate all possibilities for all random tiles
         for row in range(0, len(gameArray)):
            for col in range(0, len(gameArray[row])):
               if 0 == gameArray[row][col]:
                  # random tile could spawn here; test all possible values
                  for val in range(2, 5, 2):
                     # generate all possibilities for the random tile being 2 
                     # and 4
                     prob = probOfNewTile(gameArray, val)
                     gameArray[row][col] = val
                     addGame = GameState(moveGame)
                     addGame.setBoard(gameArray)
                     
                     if False: # 1 <= opt:
                        # don't include repeats
                        add = True
                        for child in node.children:
                           result = compareGames(addGame, child.game)
                           if 0 != result:
                              # addGame has the same board as and the same or 
                              # worse score than child
                              # disregard addGame
                              add = False
                              child.prob += prob
                              break

                        if add:
                           # addGame either had a unique board or had the same
                           # board as a preexisting child but a better score
                           node.children += [Tree(addGame, [], Move(move), prob)]
                        
                     else:
                        node.children += [Tree(addGame, [], Move(move), prob)]

                     gameArray = moveGame.copyArr()


# given a depth > 0 and a node, generates the children of the node
def genTreeInner(depth, node, opt, nodesPerLevel):
   # generate the next level
   genLevel(node, opt)

   # if children not at bottom level, recurse down the children
   if 1 < depth:
      for child in node.children:
         if 1 <= opt:
            add = True
            replace = False
            for existing in nodesPerLevel[depth - 1]:
               if child.game.gameArray == existing.game.gameArray:
                  if child.game.score <= existing.game.score:
                     add = False
                     existing.prob += child.prob
                     node.children.remove(child)
                     break
                  else:
                     replace = True
                     child.prob += existing.prob
                     node.children.remove(existing)
                     nodesPerLevel[depth - 1].remove(existing)
                     break
               if not add or replace:
                  break
            if add:
               nodesPerLevel[depth - 1] += [child]

         genTreeInner(depth - 1, child, opt, nodesPerLevel)
   elif 1 <= opt:
      # nodesPerLevel was constructed backwards; reverse
      nodesPerLevel = list(reversed(nodesPerLevel))

# given a tree and a list of indices representing a path through the tree,
# returns the node pointed to by the path
def pathToNode(tree, path):
   if [] == path or [] == tree.children:
      return tree
   return pathToNode(tree.children[path[0]], path[1:len(path)])

# given a path to a leaf, returns the next leaf and updates the path or None if
# the given leaf is the last leaf of the tree
def nextLeaf(tree, curPath):
   prevNode = pathToNode(tree, curPath[:-1])

   if curPath[-1] < len(prevNode.children) - 1:
      # the leaf is not the last leaf belonging to its parent; return the next
      # leaf
      curPath[-1] += 1
      return prevNode.children[curPath[-1]]

   # the leaf is the last leaf of its parent; go up the tree until either find
   # an ancestor that is not the last child of its parent or hit the root
   i = -1
   while None != prevNode.prevMove:
      # while not root, go up until hit root or prevNode is not the last child
      # of its parent
      curPath[i] = 0
      i -= 1
      prevNode = pathToNode(tree, curPath[:i])

      if curPath[i] < len(prevNode.children) - 1:
         curPath[i] += 1
         return pathToNode(tree, curPath)

   return None

# given a game and a tree, returns the subtree of the tree with root equal to
# the game
def updateTree(game, tree, opt, nodesPerLevel):
   gameArray = game.copyArr()
   for child in tree.children:
      childArray = child.game.copyArr()
      if gameArray == childArray:
         child.game.score = game.score # just in case
         child.prevMove = None # mark child as the root of the new tree
         
         # update nodesPerLevel
         if 1 <= opt:
            nodesPerLevel.remove(nodesPerLevel[0])
            nodesPerLevel.remove(nodesPerLevel[0])
            nodesPerLevel.insert(0, [child])
         return child

   return None # something went wrong

### External methods--call these ###

# given a node, adds another level to the bottom of the tree below this node. 
# call this when a move is selected and the next level needs to be generated.
def genNextLevel(node, opt, nodesPerLevel):
   # this node is currently a leaf; add another level below
   if [] == node.children:
      genLevel(node, opt)

      if 1 <= opt:
         # eliminate redundancies
         nodesPerLevel += [[]]
         for child in node.children:
            add = True
            replace = False
            for existing in nodesPerLevel[-1]:
               if child.game.gameArray == existing.game.gameArray:
                  if child.game.score <= existing.game.score:
                     add = False
                     existing.prob += child.prob
                     node.children.remove(child)
                     break
                  else:
                     replace = True
                     child.prob += existing.prob
                     nodesPerLevel[-1].remove(existing)
                     break
               if not add or replace:
                  break
            if add:
               nodesPerLevel[-1] += [child]
      return

   # this node is not a leaf; recurse down
   for child in node.children:
      genNextLevel(child, opt, nodesPerLevel)


# given a depth >= 0 and a gamestate, generates a new tree. Depth = 0 returns
# a node with no children. call this to generate a full tree when none exists.
def genTree(depth, game, opt, nodesPerLevel):
   if 0 > depth:
      return None
   
   tree = Tree(GameState(game), [], None, 1)

   if 1 <= opt:
      for i in range(depth + 1):
         nodesPerLevel += [[]]
      nodesPerLevel[-1] += [tree]

   # if only a root needs to be created, return
   if 0 == depth:
      return tree

   # generate the rest of the tree
   genTreeInner(depth, tree, opt, nodesPerLevel)

   return tree

# given a GameState, returns True if a tree should be used and False otherwise
# returns only False right now for testing purposes
def treeCondition(game):
   gameArray = game.copyArr()
   if 2 < countEmpty(gameArray):
      return True

   return False

# use a tree to select the next move
def takeMoveTree(tree, nodesPerLevel, game, evals, treeDepth, logFileStream, gameNum, moveNum, opt):
   if None == tree:
      # just switched over from not using trees; generate a new tree
      tree = genTree(treeDepth, game, opt, nodesPerLevel)
   else:
      genNextLevel(tree, opt, nodesPerLevel)

   # initialize best path (last is best leaf)
   # initialize current path equal to best path
   # path[i] = the index to follow of the node at the ith level's children
   bestPath = [0] * treeDepth
   curPath = [0] * treeDepth
   bestLeaf = pathToNode(tree, bestPath)
   curLeaf = nextLeaf(tree, curPath)
   bestScore = scoreGame(bestLeaf.game, evals)
   
   # search other leaves; update best path when find a higher scoring leaf
   while None != curLeaf:
      curScore = scoreGame(curLeaf.game, evals)
      if bestScore < curScore:
         # found a new best leaf; update
         bestPath = curPath[:]
         bestLeaf = curLeaf
         bestScore = curScore

      curLeaf = nextLeaf(tree, curPath)

   # take the move specified by the path to the best leaf
   game.takeMove(tree.children[bestPath[0]].prevMove)
   game.addRandomTile()

   # print results
   if (sys.stdout != logFileStream):
      logFileStream.write('Game {} turn {}\n'.format(gameNum, moveNum))
      logFileStream.write('Score: {}\n'.format(game.getScore()))
      logFileStream.write('Solver: tree\n')
      logFileStream.write('Move: {}\n'.format(tree.children[bestPath[0]].prevMove)) 
      game.printState(logFileStream)
      logFileStream.write('\n')

   # find subtree for game
   return updateTree(game, tree, opt, nodesPerLevel)

# Given a starting state, an action, and a state that may result from taking the
# action, returns the probability of achieving that possible result state
def probOfState(s, a, s_pp):
   # create an s' state that reflects applying action a to state s, but with no
   # random tile generation to take it to any s'' state
   s_p = GameState(s)
   s_p.takeMove(a)
   diff = countDifferences(s_p, s_pp)
   if 2 > diff:
      # s_pp should be equal to s_p except for the placement of one random tile
      # if there are > 1 differences, s_pp was not produced by taking action a
      # there is a 0% chance of getting s_pp with action a
      return 0.0

   # get the length and width of the board
   length, width = getDimensions(s)
   percent = float(countEmpty(s)) / (length * width)
   if 2 == diff:
      return percent * .9
   
   return percent * .1

### Test methods ###
def testGenLevel():
   game = GameState()
   game.takeMove(Move.left, True)

   tree = Tree(game, [], None, 1)
   genLevel(tree, 0)

   print('Original game:')
   game.printState(stdout)

   print('\n\nAll possible moves:')
   for i in range(0, len(tree.children)):
      child = tree.children[i]
      child.game.printState(stdout)
      print(i, '\n')

def testGenTreeInner():
   game = GameState()
   game.takeMove(Move.left, True)

   tree = Tree(game, [], None, 1)
   genTreeInner(1, tree, 0)
   print(tree.game, tree.children)
   for child in tree.children:
      print("\n", child.children)

def testGenTree():
   game = GameState()
   game.takeMove(Move.left, True)

   tree = genTree(2, game, 0, 1)
   print(tree.game, tree.children)
   for child in tree.children:
      print("\n", child.children)

def testGenNextLevel():
   game = GameState()
   game.takeMove(Move.left, True)

   tree = genTree(1, game, 0, 1)
   for child in tree.children:
      print(child.children)

   print("\n")
   
   genNextLevel(tree, 0)
   for child in tree.children:
      print(child.children)

if '__main__' == __name__:
   testGenLevel()
