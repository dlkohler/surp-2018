from GameState import *
from random import randint
import sys

### Helper functions ###

# Given an array representing the game board, returns the number of empty spaces
def countEmpty(array):
   numEmpty = 0

   for row in array:
      for col in row:
         if 0 == col:
            numEmpty += 1

   return numEmpty

### Scoring functions ###

# given a GameState, returns the greedy score (the score of the game)
def scoreGreedy(game):
   return game.getScore()

# given a GameState, returns the monotonicity score
def scoreMonotonicity(game):
   bestMonotonicity = 0
   curMonotonicity = 0

   testArray = game.copyArr()

   for row in testArray:
      for col in range(0, len(row) - 1):
         if row[col] >= row[col + 1]:
            curMonotonicity += 1

   for col in range(0, len(testArray[0])):
      for row in range(0, len(testArray[0]) - 1):
         if testArray[row][col] >= testArray[row + 1][col]:
            curMonotonicity += 1

   if bestMonotonicity < curMonotonicity:
      bestMonotonicity = curMonotonicity

   curMonotonicity = 0

   for row in testArray:
      for col in range(0, len(row) - 1):
         if row[col] <= row[col + 1]:
            curMonotonicity += 1

   for col in range(0, len(testArray[0])):
      for row in range(0, len(testArray[0]) - 1):
         if testArray[row][col] <= testArray[row + 1][col]:
            curMonotonicity += 1

   if bestMonotonicity < curMonotonicity:
      bestMonotonicity = curMonotonicity

   curMonotonicity = 0

   testArray = game.rotateClockwise()

   for row in testArray:
      for col in range(0, len(row) - 1):
         if row[col] >= row[col + 1]:
            curMonotonicity += 1

   for col in range(0, len(testArray[0])):
      for row in range(0, len(testArray[0]) - 1):
         if testArray[row][col] >= testArray[row + 1][col]:
            curMonotonicity += 1

   if bestMonotonicity < curMonotonicity:
      bestMonotonicity = curMonotonicity

   curMonotonicity = 0

   for row in testArray:
      for col in range(0, len(row) - 1):
         if row[col] <= row[col + 1]:
            curMonotonicity += 1

   for col in range(0, len(testArray[0])):
      for row in range(0, len(testArray[0]) - 1):
         if testArray[row][col] <= testArray[row + 1][col]:
            curMonotonicity += 1

   if bestMonotonicity < curMonotonicity:
      bestMonotonicity = curMonotonicity

   return bestMonotonicity

# given a GameState, returns the uniformity score for the game
def scoreUniformity(game):
   testArray = game.copyArr()
   numUniform = 0
   values = {}

   for row in testArray:
      for value in row:
         # get the number of times the value has occurred and increment it by 1
         numOccurred = values.get(value, 0)
         values.update({value : numOccurred + 1})

   # calculate a value based on the number of repeats
   for value in values.values():
      numUniform += (value - 1) ** 3

   return numUniform

# given a GameState, returns the empty score for the game (the number of empty 
# squares)
def scoreEmpty(game):
   return countEmpty(game.copyArr())

# given a GameState and a list of evaluators, returns a score for the game
# adjusted to give more precedence to evaluators earlier in the list
# assumption: the last evaluator in the list is a terminal evaluator and does
# not need to be scored
def scoreGame(game, evals):
   score = 0
   scorers = {'greedy' : scoreGreedy,
              'monotonicity' : scoreMonotonicity,
              'uniformity' : scoreUniformity,
              'empty' : scoreEmpty}
   for i in range(len(evals) - 1):
      score += pow(scorers[evals[i]](game), len(evals) - 2 - i)

   return score
      
### Evaluators ###

# given a game and a set of moves, returns a list of moves that will produce the highest score
def greedy(game, moves):
   bestScore = -1
   bestMoves = []

   for curMove in moves:
      if game.isValid(curMove):
         testGame = GameState(game)
         testGame.takeMove(curMove)
         testScore = scoreGreedy(game)

         if bestScore < testScore:
            bestScore = testScore
            bestMoves = [curMove]
         elif bestScore == testScore:
            bestMoves.append(curMove)

   return bestMoves

# given a game and a set of moves, returns a list of moves that will produce the most rows and
# columns in decreasing order
def monotonicity(game, moves):
   bestMonotonicity = -1
   bestMoves = []

   for curMove in moves:
      curMonotonicity = 0

      if game.isValid(curMove):

         testGame = GameState(game)
         testGame.takeMove(curMove)

         curMonotonicity = scoreMonotonicity(testGame)

         if bestMonotonicity < curMonotonicity:
            bestMonotonicity = curMonotonicity
            bestMoves = [curMove]
         elif bestMonotonicity == curMonotonicity:
            bestMoves.append(curMove)

   return bestMoves

# given a game and a set of moves, returns a list of moves that will make the highest number of
# squares that are the same
def uniformity(game, moves):
   bestUniform = -1
   bestMoves = []

   for curMove in moves:
      if game.isValid(curMove):
         testGame = GameState(game)
         testGame.takeMove(curMove)
         numUniform = scoreUniformity(game)

         if bestUniform < numUniform:
            bestUniform = numUniform
            bestMoves = [curMove]
         elif bestUniform == numUniform:
            bestMoves.append(curMove)

   return bestMoves
      


# given a game and a set of moves, returns a list of moves that will produce the highest number
# of empty spaces
def empty(game, moves):
   bestNumEmpty = -1
   bestMoves = []

   for curMove in moves:
      if game.isValid(curMove):
         testGame = GameState(game)
         testGame.takeMove(curMove)
         numEmpty = scoreEmpty(testGame)

         if bestNumEmpty < numEmpty:
            bestNumEmpty = numEmpty
            bestMoves = [curMove]
         elif bestNumEmpty == numEmpty:
            bestMoves.append(curMove)

   return bestMoves

# given a game and a set of moves, returns a list of one randomly chosen move
def random(game, moves):
   move = moves[randint(0, len(moves) - 1)]
   while not game.isValid(move):
      move = moves[randint(0, len(moves) - 1)]
   return [move]

# maps solver names to their functions
evaluators = {'greedy' : greedy,
              'monotonicity' : monotonicity,
              'uniformity' : uniformity,
              'empty' : empty,
              'random' : random}

### Top-level methods ###

# given a game and a set of moves, tries each move in order until one works. returns None
# if error
def evaluateOrder(game, moves):
   for move in moves:
      if game.isValid(move):
         return [move]

   return None

# given a game, a set of moves, and the name of an evaluator, returns the result of
# applying the evaluator to the game and set of moves
def evaluate(game, moves, solverName):
   try:
      return evaluators[solverName](game, moves)
   except KeyError as e:
      raise NameError('{} is not an evaluator'.format(solverName))
      
# given a game, takes a move based on the order of evaluators
def takeMoveEval(game, evals, logFileStream, gameNum, moveNum, orderMoves):
   moves = [Move.up, Move.right, Move.down, Move.left]
   curEval = 0 
   curEvalName = ''

   # decide what move to take
   while 1 < len(moves) and curEval < len(evals):
      if 'order-' == evals[curEval][0:6]:
         moves = evaluateOrder(game, orderMoves)
      else:
         moves = evaluate(game, moves, evals[curEval])
      curEvalName = evals[curEval]
      curEval += 1

   # take the best move
   game.takeMove(moves[0])

   # Add random tile
   game.addRandomTile()

   # print results
   if (sys.stdout != logFileStream):
      logFileStream.write('Game {} turn {}\n'.format(gameNum, moveNum))
      logFileStream.write('Score: {}\n'.format(game.getScore()))
      logFileStream.write('Solver: {}\n'.format(curEvalName))
      logFileStream.write('Move: {}\n'.format(moves[0])) 
      game.printState(logFileStream)
      logFileStream.write('\n')
