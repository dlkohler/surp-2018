import sys
import numpy
from itertools import permutations
from Evaluators import *
from Tree import *

# prints the command line arguments
def printCommandLine():
   print("python3 Driver.py strategy-file num-trials [output-file [log-file]]")

# given the filename of the strategy file, builds a list of evaluators
def parseStratFile(fileName):
   stratFile = open(fileName, 'r')
   evalList = []
   for line in stratFile:
      for word in line.split():
         evalList.append(word)

   return evalList

# Generates mutable permutations of the given evaluators
def genPermutationsNoTerminal(evals):
   if 1 == len(evals):
      return [evals]

   recurse = genPermutationsNoTerminal(evals[1:len(evals)])
   perms = []

   for perm in recurse:
      for i in range(0, len(perm) + 1):
         perms.insert(0, perm[:])
         perms[0].insert(i, evals[0])

   return perms

# Generates mutable permutations of the given evaluators and 
def genPermutations(evals, permute, random):
   perms = []

   if permute:
      permsNoTerminal = genPermutationsNoTerminal(evals)

      if random:
         # add random to all permutations, omit all strategies behind random
         for perm in permsNoTerminal:
            for i in range(1, len(perm) + 1):
               newPerm = perm[0:i]
               newPerm.append('random')
               if not newPerm in perms:
                  perms.append(newPerm)
      else:
         # generate all orders
         orders = ['order-' + ''.join(p) for p in permutations('UDLR')]
         # add all orders to all permiutations, omit all strategies behind an order
         for perm in permsNoTerminal:
            for i in range(1, len(perm) + 1):
               for order in orders:
                  newPerm = perm[0:i]
                  newPerm.append(order)
                  if not newPerm in perms:
                     perms.append(newPerm)

      # add only-random strategy
      perms.insert(0, ['random'])

   else:
      if random:
         evals.append('random')
         perms = [evals]
      else:
         orders = ['order-' + ''.join(p) for p in permutations('UDLR')]
         for order in orders:
            newPerm = evals[:]
            newPerm.append(order)
            perms.append(newPerm)

   return perms

# given an order permutation, decode the string and return the ordered list of moves
def decodeOrder(directionString):
   order = []

   if 4 != len(directionString):
      raise ValueError('Order must have four moves ({})'.format(directionString))

   for i in range(0, len(directionString)):
      if directionString[i] == 'U':
         order.append(Move.up)
      elif directionString[i] == 'L':
         order.append(Move.left)
      elif directionString[i] == 'D':
         order.append(Move.down)
      elif directionString[i] == 'R':
         order.append(Move.right)
      else:
         raise ValueError('Invalid order move: ' + directionString[i])

   return order

# for one permutation, run all games
def runPermutation(evals, num_trials, outputFileStream, logFileStream,
                   spreadFileStream, random, treeDepth, opt):
   # saves stats for all games
   scores = []

   # if uses order, decode order and create move listing to be passed into evaluator
   orderMoves = []
   if not random:
      for i in range(0, len(evals)):
         if 'order-' == evals[i][0:6]:
            orderMoves = decodeOrder(evals[i][6:len(evals[i])])

   # run all games
   for gameNum in range(1, num_trials + 1):
      moveNum = 1
      game = GameState()
      tree = None
      nodesPerLevel = []

      if sys.stdout != logFileStream:
         logFileStream.write('Game {}\n\n'.format(gameNum))
         game.printState(logFileStream)
         logFileStream.write('\n')

      while game.isGoing():
         if 0 < treeDepth: # and treeCondition(game):
            tree = takeMoveTree(tree, nodesPerLevel, game, evals, treeDepth,
                                logFileStream, gameNum, moveNum, opt)
               
         else:
            tree = None
            takeMoveEval(game, evals, logFileStream, gameNum, moveNum, orderMoves)

         moveNum += 1

      if (sys.stdout != logFileStream):
         logFileStream.write("Score: {}\n".format(game.getScore()))

      # update stats
      scores.append(game.getScore())

   mean = numpy.mean(scores)
   stddev = numpy.std(scores)
   median = numpy.median(scores)
   max = numpy.max(scores)

   outputFileStream.write('   Average: {}\n'.format(mean))
   outputFileStream.write('   Std Dev: {}\n'.format(stddev))
   outputFileStream.write('   Median: {}\n'.format(median))
   outputFileStream.write('   Max: {}\n'.format(max))

   if (sys.stdout != logFileStream):
      logFileStream.write('   Average: {}\n'.format(mean))
      logFileStream.write('   Std Dev: {}\n'.format(stddev))
      logFileStream.write('   Median: {}\n'.format(median))
      logFileStream.write('   Max: {}\n'.format(max))

   if (sys.stdout != spreadFileStream):
      spreadFileStream.write('{} '.format(mean))
      spreadFileStream.write('{} '.format(stddev))
      spreadFileStream.write('{} '.format(median))
      spreadFileStream.write('{}\n'.format(max))

def main():
   # check command line for valid length
   if 3 > len(sys.argv) or 10 < len(sys.argv):
      printCommandLine()
      return

   # collect info from sys.argv
   strategy_filename = sys.argv[1]
   num_trials = int(sys.argv[2])
   outputFileStream = sys.stdout # if output file specified, will point to that file
   logFileStream = sys.stdout # if log file specified, will point to that file
   size = 4
   random = False
   permute = True
   spreadFileStream = sys.stdout # if output file specified, will make a spreadsheet-friendly file with
                                 # a similar name
   outputFileName = '' # used in case -spread is set
   treeDepth = 0 # depth of the tree
   opt = 0 # optimization level
   noTerm = False # if True, will not add random or order strategies to the end
                  # of strategies


   # collect info for optional args output file and log file
   for i in range(3, len(sys.argv)):
      if '-o=' == sys.argv[i][0:3]:
         outputFileName = sys.argv[i][3:]
         outputFileStream = open(outputFileName, 'w+')
      elif '-l=' == sys.argv[i][0:3]:
         logFileStream = open(sys.argv[i][3:], 'w+')
      elif '-n=' == sys.argv[i][0:3]:
         try:
            size = int(sys.argv[i][3:])
         except:
            raise ValueError('size must be an integer greater than 1')
         if 1 >= size:
            raise ValueError('size must be greater than 1')
      elif '-random' == sys.argv[i]:
         random = True
      elif '-nopermute' == sys.argv[i]:
         permute = False
      elif '-td=' == sys.argv[i][0:4]:
         try:
            treeDepth = int(sys.argv[i][4:])
         except:
            raise ValueError('tree depth must be an integer >= 1')
         if 0 >= treeDepth:
            raise ValueError('tree depth must be an integer >= 1')
      elif '-noterm' == sys.argv[i]:
         noTerm = True
      elif '-opt=' == sys.argv[i][0:5]:
         try:
            opt = int(sys.argv[i][5])
         except:
            raise ValueError('optimization level must be an integer > 0')
         if 0 > opt:
            raise ValueError('optimization level must be an integer > 0')
      elif '-spread' == sys.argv[i] and outputFileName != '':
         spreadFileStream = open(outputFileName + '.spread', 'w+')
      elif '-spread' != sys.argv[i]:
         raise ValueError('unknown parameter: {}'.format(sys.argv[i]))

   # get the list of solvers from the strategy file
   evals = parseStratFile(strategy_filename)

   # generate permutations
   if noTerm:
      perms = [evals]
   else:
      perms = genPermutations(evals, permute, random)

   # if log file specified, list all solvers
   if sys.stdout != logFileStream:
      logFileStream.write('Solvers:\n')
      for i in range(0, len(evals)):
         logFileStream.write('{}\n'.format(evals[i]))
      logFileStream.write('\n\n')

   # run all permutations
   permNum = 1
   for perm in perms:
      # print permutation info
      outputFileStream.write('Permutation {}:\n'.format(permNum))
      for eval in perm:
         outputFileStream.write('   {}\n'.format(eval))
      outputFileStream.write('\n')

      if sys.stdout != logFileStream:
         logFileStream.write("Permutation {}:\n".format(permNum))
         for eval in perm:
            logFileStream.write('   {}\n'.format(eval))
         logFileStream.write('\n')

      if sys.stdout != spreadFileStream:
         for eval in perm:
            if "order-" != eval[0:6]:
               spreadFileStream.write(eval[0])
            else:
               spreadFileStream.write(eval[0] + eval[6:])
         spreadFileStream.write(' ')


      runPermutation(perm, num_trials, outputFileStream, logFileStream, 
                     spreadFileStream, random, treeDepth, opt)
      permNum += 1

      outputFileStream.write("\n\n\n")
      if sys.stdout != logFileStream:
         logFileStream.write("\n\n\n")

if __name__ == '__main__':
   main()
