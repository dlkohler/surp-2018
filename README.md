## Running strategy
python3 Driver.py <STRATEGY_FILE> <NUM_ITERATIONS> [-o=<OUTPUT_FILE>] [-l=<LOG_FILE>] 
                  [-n=<SIZE>] [-random] [-nopermute] [-spread] [-td=<DEPTH>] [-opt=<OPT>] [-noterm]
  
   <STRATEGY_FILE>: sets the name of the strategy file  
   <NUM_ITERATIONS>: sets the number of iterations per strategy  
   -o=<OUTPUT_FILE>: writes results to an output file, with the name specified on the right side of the =. If not specified, writes to stdout  
   -l=<LOG_FILE>: writes a more detailed log to the file specified  
   -n=<SIZE>: sets the size of the board (default: 4)  
   -random: instead of ending each strategy with a terminal order evaluator, ends each strategy with a terminal random evaluator  
   -nopermute: instead of generating permutations of the evaluators given in STRATEGY_FILE, just tests the ordering as listed  
   -spread: creates spreadsheet-friendly output in the file <OUTPUT_FILE>.spread  
   -td=<DEPTH>: specifies the depth of the tree (must be >= 0; default: 0)  
   -opt=<OPT>: determines optimization level (must be >= 0; default 0; currently supports <= 1)  
   -noterm: solver will not add any random or order solvers to the strategy. Use only if the random or order strategies are specifically
            included in the strategy  
  
## Running adverserial
python3 Driver.py STRATEGY_FILE NUM_ITERATIONS [OUTPUT_FILE [LOG_FILE]]
